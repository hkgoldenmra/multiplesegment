#include <MultipleSegment.h>

byte anodePins[] = {8, 7, 6, 5, 4, 3, 2};
byte cathodePins[] = {A4, A3, A2, A1, A0};

MultipleSegment mp = MultipleSegment(sizeof(anodePins) / sizeof(byte), anodePins, sizeof(cathodePins) / sizeof(byte), cathodePins);
byte x = 0;
byte digits[10][7] = {
	{0, 1, 2, 3, 4, 5, 9},
	{1, 2, 9, 9, 9, 9, 9},
	{0, 1, 3, 4, 6, 9, 9},
	{0, 1, 2, 3, 6, 9, 9},
	{1, 2, 5, 6, 9, 9, 9},
	{0, 2, 3, 5, 6, 9, 9},
	{0, 2, 3, 4, 5, 6, 9},
	{0, 1, 2, 9, 9, 9, 9},
	{0, 1, 2, 3, 4, 5, 6},
	{0, 1, 2, 3, 5, 6, 9},
};

void setup() {
	mp.initial();
}

void loop() {
	const byte m = x / 60;
	const byte s = x % 60;
	for (byte i = 0; i < 2; i++) {
		for (byte j = 0; j < 100; j++) {
			{
				const byte y = m / 10 % 10;
				byte cathode[] = {4};
				mp.set(sizeof(digits[y]) / sizeof(byte), digits[y], sizeof(cathode) / sizeof(byte), cathode);
			}
			{
				const byte y = m / 1 % 10;
				byte cathode[] = {3};
				mp.set(sizeof(digits[y]) / sizeof(byte), digits[y], sizeof(cathode) / sizeof(byte), cathode);
			}
			{
				const byte y = s / 10 % 10;
				byte cathode[] = {2};
				mp.set(sizeof(digits[y]) / sizeof(byte), digits[y], sizeof(cathode) / sizeof(byte), cathode);
			}
			{
				const byte y = s / 1 % 10;
				byte cathode[] = {1};
				mp.set(sizeof(digits[y]) / sizeof(byte), digits[y], sizeof(cathode) / sizeof(byte), cathode);
			}
			if (i % 2 == 0) {
				byte anodes[] = {0, 4, 5};
				byte cathode[] = {0};
				mp.set(sizeof(anodes) / sizeof(byte), anodes, sizeof(cathode) / sizeof(byte), cathode);
			} else {
				byte anodes[] = {0, 5};
				byte cathode[] = {0};
				mp.set(sizeof(anodes) / sizeof(byte), anodes, sizeof(cathode) / sizeof(byte), cathode);
			}
		}
	}
	x++;
}