#include <MultipleSegment.h>

const byte SEGMENTS[16][8] = {
	{0, 1, 2, 3, 4, 5, 5, 5}, // 0
	{1, 2, 2, 2, 2, 2, 2, 2}, // 1
	{0, 1, 3, 4, 6, 6, 6, 6}, // 2
	{0, 1, 2, 3, 6, 6, 6, 6}, // 3
	{1, 2, 5, 6, 6, 6, 6, 6}, // 4
	{0, 2, 3, 5, 6, 6, 6, 6}, // 5
	{0, 2, 3, 4, 5, 6, 6, 6}, // 6
	{0, 1, 2, 2, 2, 2, 2, 2}, // 7
	{0, 1, 2, 3, 4, 5, 6, 6}, // 8
	{0, 1, 2, 3, 5, 6, 6, 6}, // 9
	{0, 1, 2, 4, 5, 6, 6, 6}, // A
	{2, 3, 4, 5, 6, 6, 6, 6}, // b
	{0, 3, 4, 5, 5, 5, 5, 5}, // C
	{1, 2, 3, 4, 6, 6, 6, 6}, // d
	{0, 3, 4, 5, 6, 6, 6, 6}, // E
	{0, 4, 5, 6, 6, 6, 6, 6}, // F
};

byte anodePins[] = {10};
byte cathodePins[] = {6, 5, 4, 3, 2, 7, 8, 9};

MultipleSegment mp = MultipleSegment(sizeof(anodePins) / sizeof(byte), anodePins, sizeof(cathodePins) / sizeof(byte), cathodePins);
byte x = 0;

void setup() {
	mp.initial();
}

void loop() {
	x %= sizeof(SEGMENTS) / sizeof(SEGMENTS[0]);
	byte anodes[] = {0};
	for (unsigned int i = 0; i < 2; i++) {
		mp.setCathode(7, i % 2 == 1);
		for (unsigned int j = 0; j < 500; j++) {
			mp.set(sizeof(anodes) / sizeof(byte), anodes, sizeof(SEGMENTS[x]) / sizeof(byte), SEGMENTS[x]);
		}
	}
	x++;
}