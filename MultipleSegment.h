#ifndef MULTIPLESEGMENT_H
#define MULTIPLESEGMENT_H

#include <Arduino.h>

class MultipleSegment {
	public:
		static const unsigned int DELAY_US = 500;
		static const byte MAX_LENGTH = 32;
		MultipleSegment(byte, byte[], byte, byte[]);
		void initial();
		byte getAnodeLength();
		byte getCathodeLength();
		void setAnode(byte, bool);
		void setAnode(byte, byte[], bool);
		void setAnode(bool);
		void setCathode(byte, bool);
		void setCathode(byte, byte[], bool);
		void setCathode(bool);
		void set(byte, byte);
		void set(byte, byte[], byte, byte[]);
	private:
		byte anodeLength;
		byte cathodeLength;
		byte anodePins[MultipleSegment::MAX_LENGTH];
		byte cathodePins[MultipleSegment::MAX_LENGTH];
};

#endif