#include <MultipleSegment.h>

MultipleSegment::MultipleSegment(byte anodeLength, byte anodePins[], byte cathodeLength, byte cathodePins[]) {
	memcpy(this->anodePins, anodePins, this->anodeLength = anodeLength);
	memcpy(this->cathodePins, cathodePins, this->cathodeLength = cathodeLength);
}

void MultipleSegment::initial() {
	for (byte i = 0; i < this->anodeLength; i++) {
		pinMode(this->anodePins[i], OUTPUT);
		this->setAnode(i, LOW);
	}
	for (byte i = 0; i < this->cathodeLength; i++) {
		pinMode(this->cathodePins[i], OUTPUT);
		this->setCathode(i, HIGH);
	}
}

void MultipleSegment::setAnode(byte index, bool enabled) {
	digitalWrite(this->anodePins[index], enabled);
}

void MultipleSegment::setAnode(byte length, byte indices[], bool enabled) {
	for (byte i = 0; i < length; i++) {
		this->setAnode(indices[i], enabled);
	}
}

void MultipleSegment::setAnode(bool enabled) {
	this->setAnode(this->anodeLength, this->anodePins, enabled);
}

void MultipleSegment::setCathode(byte index, bool enabled) {
	digitalWrite(this->cathodePins[index], enabled);
}

void MultipleSegment::setCathode(byte length, byte indices[], bool enabled) {
	for (byte i = 0; i < length; i++) {
		this->setCathode(indices[i], enabled);
	}
}

void MultipleSegment::setCathode(bool enabled) {
	this->setCathode(this->cathodeLength, this->cathodePins, enabled);
}

void MultipleSegment::set(byte anodeIndex, byte cathodeIndex) {
	this->setAnode(anodeIndex, HIGH);
	this->setCathode(cathodeIndex, LOW);
	delayMicroseconds(MultipleSegment::DELAY_US);
	this->setAnode(anodeIndex, LOW);
	this->setCathode(cathodeIndex, HIGH);
	delayMicroseconds(MultipleSegment::DELAY_US);
}

void MultipleSegment::set(byte anodeLength, byte anodeIndices[], byte cathodeLength, byte cathodeIndices[]){
	this->setAnode(anodeLength, anodeIndices, HIGH);
	this->setCathode(cathodeLength, cathodeIndices, LOW);
	delayMicroseconds(MultipleSegment::DELAY_US);
	this->setAnode(anodeLength, anodeIndices, LOW);
	this->setCathode(cathodeLength, cathodeIndices, HIGH);
	delayMicroseconds(MultipleSegment::DELAY_US);
}